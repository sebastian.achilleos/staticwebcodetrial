package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class})
@WebAppConfiguration
public class HelloControllerTest {
 
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
 
    @Test
    public void runHello() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }
    
    @Test
    public void runHelloName() throws Exception {

        mockMvc.perform(get("/hello/Prashant+Beniwal"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }

    @Test
    public void runNegativeScen() throws Exception {

        mockMvc.perform(get("/try/Prashant+Beniwal"))
                .andExpect(status().isNotFound())
                ;
    }
	
    @Test
    public void calculatorAdd() {

    Calculator calculator = new Calculator();
    assertEquals(calculator.add(),9);
    
    }

    @Test
    public void calculatorSub() {
   
    Calculator calc = new Calculator();
    assertEquals(calc.sub(),3);
    
    }
   
    @Test
    public void firstIncrement() {

    Increment inc = new Increment();

    assertEquals(inc.decreasecounter(0),1);

    }

    @Test 
    public void secondIncrement() {

    Increment inc = new Increment();

    assertEquals(inc.decreasecounter(1),1);

    }

    @Test
    public void thirdIncrement() {

    Increment inc = new Increment();

    assertEquals(inc.decreasecounter(10),1);
    }
}
